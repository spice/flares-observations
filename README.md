# STIX flare catalog and associated SPICE observations

This script, which is under development,lists Solar Orbiter/[SPICE](https://spice.osups.universite-paris-saclay.fr/) observations (when they exist) for flares from the Solar Orbiter/[STIX](https://datacenter.stix.i4ds.net/) [flare list](https://github.com/hayesla/stix_flarelist_science/).

How to use:

* Create and activate a `venv` and install requirements
* Download flare list to local directory
* `python script/cross_match`: get matches, save them and save a joint catalog
* `python script/publish`: create HTML file and images for the joint catalog
