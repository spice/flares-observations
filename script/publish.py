"""
Publish joint STIX flare list and SPICE observations catalog.
"""

import pandas as pd
from pathlib import Path
import jinja2
import matplotlib.pyplot as plt
import markdown
from PIL import Image

from astropy.coordinates import SkyCoord
import astropy.units as u
from sospice import Catalog, FileMetadata, plot_fovs_with_background

from cross_match import (
    matches_file,
    spice_release,
    flares_columns,
    spice_columns,
    paths,
    get_catalogs,
    read_matches,
    get_joint_catalog,
    get_flares_observations,
)


def render_html(data, template):
    """
    Render HTML file from template using data.
    """
    jinjaEnv = jinja2.Environment(
        loader=jinja2.FileSystemLoader(searchpath=paths["templates"]), autoescape=True
    )
    md = markdown.Markdown(extensions=["meta"])
    jinjaEnv.filters["markdown"] = lambda text: jinja2.utils.markupsafe.Markup(
        md.convert(text)
    )
    template = jinjaEnv.get_template(template)
    html = template.render(**data)
    return html


def get_fig_filename(key, background_key, thumbnail=False):
    thumb = "-thumb" if thumbnail else ""
    return Path(f"flare{key}-{background_key}-spice-fovs{thumb}.png")


def get_fig_filepath(key, background_key, thumbnail=False):
    return paths["html"] / get_fig_filename(key, background_key, thumbnail)


def write_fig_fov(flares, spice_cat, matches):
    # generate SPICE FOV plot with EUI/FSI background and STIX flare location
    background_key = "eui"
    background = {
        "eui": "EUI/FSI",
        "hmi": "HMI_synoptic",
    }[background_key]
    flares_observations = get_flares_observations(flares, matches)
    for key in flares_observations:
        print(key, end="... ")
        flare = flares.loc[key]
        flare_observations = flares_observations[key]
        spice_observations = Catalog(data_frame=spice_cat.loc[flare_observations])
        observer = FileMetadata(
            spice_observations.loc[flare_observations[0]]
        ).get_observer()
        flare_location = SkyCoord(
            flare["hpc_x_solo"] * u.arcsec,
            flare["hpc_y_solo"] * u.arcsec,
            observer=observer,
            obstime=flare["peak_UTC"],
            frame="helioprojective",
        )
        fig_path = get_fig_filepath(key, background_key)
        if fig_path.exists():
            print("Already exists, skipping")
            continue
        try:
            fig, ax = plot_fovs_with_background(
                spice_observations, background, show=False
            )
            ax.plot_coord(flare_location, "x", color="r", label="Flare location")
            fig.savefig(fig_path)
            plt.close()
            thumb_path = get_fig_filepath(key, background_key, thumbnail=True)
            im = Image.open(fig_path)
            im.thumbnail((60, 60))
            im.save(thumb_path)
        except RuntimeError:
            print("Probably no EUI")
            continue
        except ValueError:
            print("Probably no HMI synoptic map")
            continue


def write_html(flares, spice_cat, matches):
    """
    Write HTML table from catalogs and matches

    Parameters
    ----------
    flares: pandas.DataFrame
        STIX flare list
    spice_cat: pandas.DataFrame
        SPICE observations catalog
    matches: list of tuples
        Matches
    """
    flares_observations = get_flares_observations(flares, matches, include_empty=True)
    spice_studies = list()
    spice_thumbnails = list()
    for irow, row in flares.iterrows():
        studies = [spice_cat.loc[fo].STUDY for fo in flares_observations[irow]]
        counted_studies = [
            (studies.count(study), study) for study in sorted(list(set(studies)))
        ]
        text_studies = [
            study + (f" ({n_study})" if n_study > 1 else " ")
            for n_study, study in counted_studies
        ]
        spice_studies.append((irow, ", ".join(text_studies)))
        background_key = "eui"
        fig_path = get_fig_filepath(irow, background_key)
        thumb_path = get_fig_filepath(irow, background_key, thumbnail=True)
        if fig_path.exists() and thumb_path.exists():
            spice_thumbnails.append(
                (
                    irow,
                    f"[![SPICE FOV on EUI background]({thumb_path.name})]({fig_path.name})",
                )
            )
        else:
            spice_thumbnails.append((irow, ""))
    spice_studies = pd.DataFrame(
        spice_studies, columns=["index", "spice_studies"]
    ).set_index("index")
    spice_thumbnails = pd.DataFrame(
        spice_thumbnails, columns=["index", "SPICE FOV"]
    ).set_index("index")
    table = pd.concat([flares[flares_columns], spice_studies, spice_thumbnails], axis=1)
    if True:   # remove rows with no SPICE studies
        table = table[table.spice_studies != ""]
    for column in table.columns:
        if "UTC" in column:
            table[column] = table[column].apply(
                lambda t: t.isoformat(timespec="seconds")
            )
        else:
            table[column] = table[column].apply(str)
    template_data = {
        "now": pd.Timestamp("now", tz="UTC").isoformat(timespec="seconds"),
        "table": table,
    }
    html = render_html(template_data, template="index.html")
    paths["html"].mkdir(exist_ok=True)
    with open(paths["html"] / "index.html", "w") as f:
        f.write(html)
    for d in ["css", "js"]:
        if not (Path(paths["html"]) / d).exists():
            (Path(paths["html"]) / d).symlink_to(Path("..") / paths["assets"] / d)
    # TODO Pages for individual flares


if __name__ == "__main__":
    print("Reading catalogs and matches")
    flares, spice_cat = get_catalogs()
    assert Path(matches_file).exists()
    matches = read_matches()
    print("Generating web page and images")
    write_html(flares, spice_cat, matches)
    write_fig_fov(flares, spice_cat, matches)
