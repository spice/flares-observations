"""
Cross-match STIX flare list and SPICE observations catalog.
"""

import pandas as pd
from tqdm import tqdm
from pathlib import Path

from sospice import Catalog, FileMetadata
from sospice.catalog.file_metadata import required_columns as spice_useful_columns


# parameters
stix_flare_list = "STIX_flarelist_w_locations_20210214_20230928_version1_NOV.csv"
spice_release = "4.0"
matches_file = "matches.csv"

# columns for cross-matched summary catalog
flares_columns = [
    "start_UTC",
    "end_UTC",
    "peak_UTC",
    "hpc_x_solo",
    "hpc_y_solo",
    "4-10 keV",
]
spice_columns = ["DATE-BEG", "DATE-END", "SPIOBSID", "RASTERNO", "STUDY"]

paths = {
    "html": Path("html"),
    "assets": Path("html-assets"),
    "templates": Path("templates"),
}


def get_catalogs():
    """
    Read catalogs and generate ancillary columns

    Return
    ------
    pandas.DataFrame
        Flare catalog
    pandas.DataFrame
        SPICE observations catalog
    """
    flares_date_columns = ["start_UTC", "end_UTC", "peak_UTC"]
    flares = pd.read_csv(stix_flare_list, parse_dates=flares_date_columns)
    flares.sort_values("peak_UTC")
    spice_cat = Catalog(release_tag=spice_release)
    spice_cat = Catalog(data_frame=spice_cat[list(spice_useful_columns)])
    spice_cat = spice_cat.find_files(level="L2")
    spice_cat.sort_values("DATE-BEG")
    spice_cat["DATE-END"] = spice_cat.apply(spice_end_date, axis=1)
    return flares, spice_cat


def spice_fov_bounding_box(spice_row):
    """
    SPICE FOV bounding box: minimum and maximum helioprojective x and y from SPICE FOV
    (rectangle aligned with helioprojective coordinates, ignoring FOV rotation)

    Parameters
    ----------
    spice_row: pandas.Series
        SPICE catalog row

    Return
    ------
    tuple
        Ranges for helioprojective x and y
    """
    fov = FileMetadata(spice_row).get_fov(points=2)
    xrange = (fov.Tx.min().value, fov.Tx.max().value)
    yrange = (fov.Ty.min().value, fov.Ty.max().value)
    return xrange, yrange


def spice_end_date(spice_row):
    """
    End of SPICE observation

    Parameters
    ----------
    spice_row: sospice.FileMetadata
        SPICE catalog row

    Return
    ------
    pandas.Timestamp
        End of SPICE observation
    """
    return spice_row["DATE-BEG"] + pd.Timedelta(spice_row["TELAPSE"], "seconds")


def match_date(flares_row, spice_row):
    """
    Check whether flare and SPICE observation dates match

    Parameters
    ----------
    flares_row: pandas.Series
        Flare
    spice_row: pandas.Series
        SPICE observation

    Return
    ------
    bool
        True if match
    """
    observation_ends_after_flare_starts = (
        spice_row["DATE-END"] > flares_row["start_UTC"]
    )
    observation_starts_before_flare_ends = spice_row["DATE-BEG"] < flares_row["end_UTC"]
    return observation_ends_after_flare_starts and observation_starts_before_flare_ends


def match_coordinates(flares_row, spice_row):
    """
    Check whether flare and SPICE observation coordinates match

    Parameters
    ----------
    flares_row: pandas.Series
        Flare
    spice_row: pandas.Series
        SPICE observation

    Return
    ------
    bool
        True if match
    """
    xrange, yrange = spice_fov_bounding_box(spice_row)
    flare_in_xrange = xrange[0] < flares_row.hpc_x_solo < xrange[1]
    flare_in_yrange = yrange[0] < flares_row.hpc_y_solo < yrange[1]
    return flare_in_xrange and flare_in_yrange


def cross_match(flares, spice_cat):
    """
    Cross-match STIX flares list and SPICE observations catalog

    Parameters
    ----------
    flares: pandas.DataFrame
        STIX flare list
    spice_cat: pandas.DataFrame
        SPICE observations catalog

    Return
    ------
    list
        List of (flares index, spice_cat index) tuples

    Naive algorithm: for each flare, look for matches in all SPICE catalog.
    """
    matches = list()
    for flares_index, flares_row in flares.iterrows():
        spice_mask = spice_cat.apply(
            lambda spice_row: match_date(flares_row, spice_row), axis=1
        )
        for spice_index in spice_mask[spice_mask].index:
            matches.append((flares_index, spice_index))
    # Down-select by coordinates
    matches_dates_coords = list(
        filter(
            lambda m: match_coordinates(flares.loc[m[0]], spice_cat.loc[m[1]]), matches
        )
    )
    return matches_dates_coords


def efficient_cross_match(flares, spice_cat):
    """
    Efficient cross-match: after slicing catalogs by smaller date ranges

    Parameters
    ----------
    flares: pandas.DataFrame
        STIX flare list
    spice_cat: pandas.DataFrame
        SPICE observations catalog

    Return
    ------
    list
        List of (flares index, spice_cat index) tuples
    """
    date_slice_step = pd.Timedelta(days=30)
    date_slice_margin = pd.Timedelta(days=10)

    flares_dates = flares["peak_UTC"]
    spice_dates = spice_cat["DATE-BEG"]
    min_date, max_date = flares_dates.min(), flares_dates.max()
    n_slices = (
        int((max_date - min_date).total_seconds() // date_slice_step.total_seconds())
        + 1
    )
    dates_slices = [
        (min_slice := min_date + i * date_slice_step, min_slice + date_slice_step)
        for i in range(n_slices)
    ]
    matches = list()
    for dates_slice in tqdm(dates_slices):
        slice_flares = flares[
            (flares_dates >= dates_slice[0]) & (flares_dates < dates_slice[1])
        ]
        slice_spice = spice_cat[
            (spice_dates > dates_slice[0] - date_slice_margin)
            & (spice_dates < dates_slice[1] + date_slice_margin)
        ]
        matches += cross_match(slice_flares, slice_spice)
    return matches


def save_matches(matches):
    """
    Save matches as pairs of indices

    Parameters
    ----------
    matches: list of tuples
        Matches
    """
    pd.DataFrame(matches, columns=["flare_index", "spice_index"]).to_csv(matches_file)


def read_matches():
    """
    Read matches

    Return
    ------
    list of tuples
        Matches
    """
    df = pd.read_csv(matches_file)
    return list(df.apply(lambda row: (row["flare_index"], row["spice_index"]), axis=1))


def get_flares_observations(flares, matches, include_empty=False):
    """
     Get list of indices in flares list, associated to list of corresponding observations.

    Parameters
     ----------
     flares: pandas.DataFrame
         STIX flare list
     matches: list of tuples
         Matches
     include_empty: bool
         Include flares with no matches

     Return
     ------
     dict
         For each flare index: list of SPICE observation indices.
         (This is just a different representation of the list of matches).
    """
    flares_index = list(flares.index)
    flares_observations = {fi: list() for fi in flares_index}
    for m in matches:
        flares_observations[m[0]].append(m[1])
    if include_empty:
        return flares_observations
    else:
        return {k: v for k, v in flares_observations.items() if v}


def get_joint_catalog(flares, spice_cat, matches, all_columns=False):
    """
    Get joint catalog from catalogs and matches

    Parameters
    ----------
    flares: pandas.DataFrame
        STIX flare list
    spice_cat: pandas.DataFrame
        SPICE observations catalog
    matches: list of tuples
        Matches
    all_columns: bool
        Keep all columns instead of the ones for the summary table

    Return
    ------
    pandas.DataFrame
        Joint catalog
    """
    if all_columns:
        flares_cols = flares.columns
        spice_cols = spice_cat.columns
    else:
        flares_cols = flares_columns
        spice_cols = spice_columns
    joint_flares = (
        flares[flares_cols]
        .loc[[m[0] for m in matches]]
        .reset_index()
        .rename({"index": "flares_index"}, axis=1)
    )
    joint_spice = (
        spice_cat[spice_cols]
        .loc[[m[1] for m in matches]]
        .reset_index()
        .rename({"index": "spice_index"}, axis=1)
    )
    joint_catalog = pd.concat([joint_flares, joint_spice], axis=1)
    return joint_catalog


if __name__ == "__main__":
    print("Reading catalogs")
    flares, spice_cat = get_catalogs()
    print("Searching for matches")
    matches = efficient_cross_match(flares, spice_cat)
    print(f"{len(matches)} found")
    print(f"Saving matches and joint catalog")
    save_matches(matches)
    joint_catalog = get_joint_catalog(flares, spice_cat, matches)
    joint_catalog.to_csv("joint_catalog.csv")
